# SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note

include $(TOPDIR)/rules.mk

PKG_NAME:=kernel-cma
PKG_RELEASE:=$(AUTORELEASE)

PKG_MAINTAINER:=dreieck
PKG_LICENSE:=GPL-2.0

include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/package.mk

PKG_LICENSE_FILES:=$(LINUX_DIR)/COPYING
PKG_VERSION:=$(LINUX_VERSION)


_OUR_SECTION=kernel
_OUR_CATEGORY=Kernel
_OUR_SUBMENU=Contiguous Memory Allocator (CMA)


define Package/kernel-cma
	HIDDEN:=1
	SECTION=$(_OUR_SECTION)
	CATEGORY=$(_OUR_CATEGORY)
	SUBMENU:=$(_OUR_SUBMENU)
	TITLE:=CMA: Compile the kernel with Contiguous Memory Allocator (CMA)
	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
	URL:=http://kernel.org/
	KCONFIG:= \
	  CONFIG_CMA=$(if $(CONFIG_KERNEL_CMA),y,n) \
	  CONFIG_CMA_DEBUG=n \
	  CONFIG_CMA_DEBUGFS=$(CONFIG_KERNEL_DEBUG_FS)
endef

define Package/kernel-cma/config
	source "$(SOURCE)/Config.in"
endef

define Package/kernel-cma/description
	This enables the Contiguous Memory Allocator which allows other subsystems to allocate big physically-contiguous blocks of memory. CMA reserves a region of memory and allows only movable pages to be allocated from it. This way, the kernel can use the memory for pagecache and when a subsystem requests for contiguous area, the allocated pages are migrated away to serve the contiguous request.

	If unsure, say "n".
endef

$(eval $(call BuildPackage,kernel-cma))



define Package/kernel-cma-areas
	HIDDEN:=1
	SECTION=$(_OUR_SECTION)
	CATEGORY=$(_OUR_CATEGORY)
	SUBMENU:=$(_OUR_SUBMENU)
	DEPENDS:=+@KERNEL_CMA
	TITLE:=CMA: Maximum count of the CMA areas
	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
	URL:=http://kernel.org/
	KCONFIG:= \
	  CONFIG_CMA_AREAS=$(CONFIG_KERNEL_CMA_AREAS)
endef

define Package/kernel-cma-areas/description
	CMA allows to create CMA areas for particular purpose, mainly, used as device private area. This parameter sets the maximum number of CMA area in the system.

	If unsure, leave the default value "7" in UMA and "19" in NUMA.
endef

$(eval $(call BuildPackage,kernel-cma-areas))



define Package/kernel-dma-cma
	HIDDEN:=1
	SECTION=$(_OUR_SECTION)
	CATEGORY=$(_OUR_CATEGORY)
	SUBMENU:=$(_OUR_SUBMENU)
	DEPENDS:=+@KERNEL_CMA
	TITLE:=DMA_CMA: Compile the kernel with DMA Contiguous Memory Allocator (DMA_CMA)
	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
	URL:=http://kernel.org/
	KCONFIG:= \
	  CONFIG_DMA_CMA=$(if $(CONFIG_KERNEL_DMA_CMA),y,n) \
	  CONFIG_CMA_SIZE_SEL_MIN=n \
	  CONFIG_CMA_SIZE_SEL_MAX=n \
	  CONFIG_CMA_SIZE_SEL_MBYTES=y \
	  CONFIG_CMA_SIZE_SEL_PERCENTAGE=n
endef

define Package/kernel-dma-cma/description
	This enables the Contiguous Memory Allocator which allows drivers to allocate big physically-contiguous blocks of memory for use with hardware components that do not support I/O map nor scatter-gather.

	You can disable CMA by specifying "cma=0" on the kernel's command line.

	For more information see <kernel/dma/contiguous.c>.

	If unsure, say "n".
endef

$(eval $(call BuildPackage,kernel-dma-cma))



define Package/kernel-cma-size-mbytes
	HIDDEN:=1
	SECTION=$(_OUR_SECTION)
	CATEGORY=$(_OUR_CATEGORY)
	SUBMENU:=$(_OUR_SUBMENU)
	DEPENDS:=@KERNEL_DMA_CMA
	TITLE:=DMA_CMA: Default maximum contiguous memory size in MiB
	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
	URL:=http://kernel.org/
	KCONFIG:= \
	  CONFIG_CMA_SIZE_MBYTES=$(CONFIG_KERNEL_CMA_SIZE_MBYTES)
endef

define Package/kernel-cma-size-mbytes/description
	Defines the absolute size (in MiB) of the default memory area for Contiguous Memory Allocator.
	
	If the size of 0 is selected, CMA is disabled by default, but it can be enabled by passing cma=size[MG] to the kernel.
endef

$(eval $(call BuildPackage,kernel-cma-size-mbytes))



# define Package/kernel-cma-size-percentage
# 	HIDDEN:=1
# 	SECTION=$(_OUR_SECTION)
#	CATEGORY=$(_OUR_CATEGORY)
# 	SUBMENU:=$(_OUR_SUBMENU)
# 	DEPENDS:=@KERNEL_DMA_CMA
# 	TITLE:=DMA_CMA: Default maximum contiguous memory size in percent
# 	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
# 	URL:=http://kernel.org/
# 	KCONFIG:= \
# 	  CONFIG_CMA_SIZE_PERCENTAGE=$(CONFIG_KERNEL_CMA_SIZE_PERCENTAGE)
# endef
# 
# define Package/kernel-cma-size-percentage/description
# 	Defines the size of the default memory area for Contiguous Memory Allocator as a percentage of the total memory in the system.
# 	
# 	Note: The default size is calculated from this and from the default maximum absolute size in MiB: The lower value is used.
# 	If you want to just use the percentage, set the MiB-value to a very high value.
# 	
# 	If 0 percent is selected, CMA is disabled by default, but it can be enabled by passing cma=size[MG] to the kernel.
# endef
# 
# $(eval $(call BuildPackage,kernel-cma-size-percentage))



define Package/kernel-cma-alignment
	HIDDEN:=1
	SECTION=$(_OUR_SECTION)
	CATEGORY=$(_OUR_CATEGORY)
	SUBMENU:=$(_OUR_SUBMENU)
	DEPENDS:=@KERNEL_DMA_CMA
	TITLE:=DMA_CMA: Maximum PAGE_SIZE order of alignment for contiguous buffers
	VERSION:=$(LINUX_VERSION)-$(PKG_RELEASE)
	URL:=http://kernel.org/
	KCONFIG:= \
	  CONFIG_CMA_ALIGNMENT=$(CONFIG_KERNEL_CMA_ALIGNMENT)
endef

define Package/kernel-cma-alignment/description
	DMA mapping framework by default aligns all buffers to the smallest PAGE_SIZE order which is greater than or equal to the requested buffer size. This works well for buffers up to a few hundreds kilobytes, but for larger buffers it just a memory waste. With this parameter you can specify the maximum PAGE_SIZE order for contiguous buffers. Larger buffers will be aligned only to this specified order. The order is expressed as a power of two multiplied by the PAGE_SIZE.

	For example, if your system defaults to 4KiB pages, the order value of 8 means that the buffers will be aligned up to 1MiB only.

	If unsure, leave the default value "8".
endef

$(eval $(call BuildPackage,kernel-cma-alignment))
