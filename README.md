# feed-kernelsettings

## Description

This is an [OpenWrt feed](http://openwrt.org/docs/guide-developer/feeds) holding "packages" that add settings to the OpenWrt configuration menu for additional kernel configuration. Some of them also result in proper `kmod-*` kernel module packages beeing generated.

## Settings included

## Usage

To use this feeds, add to your `feeds.conf`  
```
src-git  kernelsettings  https://gitlab.com/dreieckli-openwrt/feed-kernelsettings.git
```  
and run `./scripts/feeds update -a && /scripts/feeds install -a`.

Then the packages defined here should appear in your OpenWrt configuration (`make menuconfig`). Run `make oldconfig` instead to directly be prompted about new configuration items.
